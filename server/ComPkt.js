function ComPkt(header, content)
{
    this.Header     = header;
    this.Content    = content;

    this.toString = function () {
        return JSON.stringify(this);
    }
}