var http = require('http');
var dbwatch = require('dbwatch');
var server = http.createServer(function(request, response) {});
var count = 0;
//var connections = {};
var freeIds = [];
var player  = null;
var playlist = [];
var clients = [];
var dbcheckInterval = 1 * 1000; // 10 sec.

var Headers = { InitCall: 1, ReqSong: 2, Chatter: 3, NextVid: 4, EndVid: 5, Playback: 6, Playlist: 7 };
var Connection = { Client: 1, Player: 2 };

server.listen(1235, function() {
    console.log((new Date()) + ' Server is listening on port 1235');
});

var WebSocketServer = require('websocket').server;
wsServer = new WebSocketServer({
    httpServer: server
});

wsServer.on('request', function (r) {
    // Code here to run on connection
    try {

        var connection = r.accept('echo-protocol', r.origin);
        var id = count;

        console.log("FREE LIST : " + JSON.stringify(freeIds));

        /*
        if (freeIds.length > 0) 
        {
            id = freeIds[0];
            console.log(id + " id assigned to new client");
            freeIds.splice(0, 1);

        }
        else {
            console.log(id + " new id assigned to new client");
            count++;
        }*/
        
        count++;
        
        // Create event listener
        connection.on('message', function (message) {

            var msgString = message.utf8Data;

            //console.log("Clients Says : " + msgString);

            var object = JSON.parse(msgString);

            processMessage(object, connection, id);

            /*
            var client = {};
            client.Id = id;
            client.Info = object;
            client.Conn = connection;
            clients[id] = client;

            var reply = new replyPkt(Headers.Connect, 1, "Success", "Hello?");

            connection.send(reply.toString());
            */

        });

        connection.on('close', function (reasonCode, description) {
            //var index = getClientIndexById(id);
            //clients.splice(index, 1);
            //freeIds.push(id);
			delete clients[id];
            console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.');
        });

    }
    catch (e) {
        console.log(e);
    }

});

function processMessage(object, connection, id)
{
    switch(object.Header)
    {
        case Headers.InitCall:
            acceptClient(object, connection, id);
            break;

        case Headers.ReqSong:
            processVideoRequest(object, connection, id);
            break;

        case Headers.Chatter:
            processChatter(object, connection, id);
            break;

        case Headers.NextVid:
            processNextVideo(object, connection, id);
            break;

        case Headers.EndVid:
            processPlaylist(object, connection, id);
            break;

        case Headers.Playback:
            broadcastPlayback(object, connection, id);
            break;

        default:
            console.log("Call Unknown");
            break;
            
    }
}

function acceptClient(object, connection, id)
{
    var conok = false;

    switch(object.Content)
    {
        case Connection.Player:
            console.log("Player Connected");
            var bplayer = {};
            bplayer.Conn = connection;
            player = bplayer;
            conok = true;
            break;

        case Connection.Client:
            console.log("Client Connected");
            var client = {};
            client.Id = id;
            client.Info = object;
            client.Conn = connection;
            clients[id] = client;
            conok = true;
            break;

        default:break;
    }

    if (conok) {
        var reply = new replyPkt(Headers.InitCall, 1, "Success", "Hello?");
        connection.send(reply.toString());

        if(playlist.length > 0)
        {
            console.log("sending playlist");

            var reply = new replyPkt(Headers.Playlist, 1, "Playlist", playlist);
            connection.send(reply.toString());
        }
    }
    else
    {
        var reply = new replyPkt(Headers.InitCall, 0, "Error", "Hello?");
        connection.send(reply.toString());
    }
}

function processVideoRequest(object, connection, id)
{
    var isSuccess = false;

    if (typeof object.Content != 'undefined') 
    {
        if(checkExists(object.Content.Id))
        {
            console.log("New Video Added : " + object.Content.Title);
            playlist.push(object.Content);
            isSuccess = true;
        }
        else
        {
            console.log("Video already in playlist : " + object.Content.Title);
            isSuccess = false;
        }
    }

    if (isSuccess) {
        var reply = new replyPkt(Headers.ReqSong, 1, "Success", "Video Added");
        connection.send(reply.toString());

        broadcastPlaylist();
    }
    else
    {
        var reply = new replyPkt(Headers.ReqSong, 0, "Error", "Video is in playlist");
        connection.send(reply.toString());
    }
}

function processChatter(object, connection, id)
{
    if (typeof object.Content != 'undefined')
    {
        object.Content.Client = '#' + id;
        var stringMsg = JSON.stringify(object);

        for(var index in clients)
        {
            clients[index].Conn.send(stringMsg);
        }
    }
}

function processNextVideo(object, connection, id)
{
    console.log("Player request next video");

    if (playlist.length > 0) {

        console.log("Playing : " + playlist[0].Title);
        var reply = new replyPkt(Headers.NextVid, 1, "OK", playlist[0]);
       
        connection.send(reply.toString());
    }
    else{
        var reply = new replyPkt(Headers.NextVid, 0, "EMPTY", "Playlist is Empty");
        connection.send(reply.toString());
    }
}

function processPlaylist(object, connection, id)
{
    console.log("Video playback end requested");

    if (playlist.length > 0) {

        for(var index in playlist)
        {
            if(playlist[index].Id == object.Content.Id)
            {
                console.log("Item " + object.Content.Title + "removed");
                playlist.splice(index, 1);

                broadcastPlaylist();
            }
        }
    }
}

function broadcastPlayback(object, connection, id)
{
    if (typeof object.Content != 'undefined')
    {
        for(var index in clients)
        {
            console.log("Sending to Client " + index);
            clients[index].Conn.send(JSON.stringify(object));
        }
    }
}

function broadcastPlaylist()
{
    if(playlist.length > 0)
    {
        console.log("sending playlist");

        var reply = new replyPkt(Headers.Playlist, 1, "Playlist", playlist);

        var replyString = reply.toString();

        for (var index in clients) {
        
            clients[index].Conn.send(replyString);
        }

        player.Conn.send(replyString);
    }
}

function checkExists(videoid)
{
    var isUnique = true;

    console.log("CHECKing in " + playlist.length);

    for(var index in playlist)
    {
        console.log("CHECK - " + playlist[index].Id + " : " + videoid);

        if(playlist[index].Id == videoid)
        {
            isUnique = false;
            break;
        }
    }

    return isUnique;
}

setInterval(function () {

    dbwatch.dbwatch(dbwatchCallback);

}, dbcheckInterval);

function dbwatchCallback(rows)
{

}

//support functions

function getClientIndexById(id)
{
    var index = -1;

    for(var i in clients)
    {
        if(clients.Id == id)
        {
            index = i;
        }
    }

    return index;
}

function replyPkt(header, status, message, content)
{
    this.Header = header;
    this.Status = status;
    this.Message = message;
    this.Content = content;

    this.toString = function () {
        return JSON.stringify(this);
    }
}