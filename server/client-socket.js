function SKParams(contype)
{
    this.server     = 'ws://82.113.146.75:1235';
    this.contype    = contype;

    this.onErrorCallback = null;
    this.onMessageCallback = null;
    this.onConnectedCallback = null;
    this.onDisconnectCallback = null;

    this.setConnectCallback = function(callback) {
        this.onConnectedCallback = callback;
    }

    this.setErrorCallback= function(callback) {
        this.onErrorCallback = callback;
    }

    this.setMessageCallback = function(callback) {
        this.onMessageCallback = callback;
    }

    this.setDisconnectCallback = function(callback) {
        this.onDisconnectCallback = callback;
    }
}
function CSocket(skparams)
{
    var socket;

    this.Connect = function Connect() {
        if (socket == null) {
            socket = new WebSocket(skparams.server, 'echo-protocol');

            socket.onerror = function (event) {

                console.log("WebSocket Error");

                if (typeof skparams.onErrorCallback === "function") {
                    skparams.onErrorCallback(event);
                }
                // ! after callback shoud not do anything
            };

            socket.onopen = function (event) {

                console.log("WebSocket Open");

                if (typeof skparams.onConnectedCallback === "function") {
                    skparams.onConnectedCallback(event);
                }
                // ! after callback shoud not do anything

            };

            socket.onmessage = function (event) {

                console.log("WebSocket Message Receive");

                if (typeof skparams.onMessageCallback === "function") {
                    skparams.onMessageCallback(event);
                }
                // ! after callback shoud not do anything
            };

            socket.onclose = function (event) {

                console.log("WebSocket Close");

                socket = null;

                if (typeof skparams.onDisconnectCallback === "function") {
                    skparams.onDisconnectCallback(event);
                }
                // ! after callback shoud not do anything
            };
        }
        else {
            console.log("WebSocket not null");
        }

        this.Send = function (message) {

            if (socket != null) {
                socket.send(message);
            }
        };

        this.Disconnect = function () {
            if (socket != null) {
                socket.close();
                socket = null;
            }
        };

    };
  
}